(define-module (my-services lispiec-service)
  #:use-module (guix gexp)
  #:use-module (guix deprecation)
  #:use-module (guix diagnostics)
  #:use-module (guix i18n)
  #:use-module (gnu services)
  #:use-module (gnu services admin)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services admin)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base)
  #:use-module (gnu system setuid)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages readline)
  #:use-module (guix records)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 binary-ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (my-packages extra)
  #:export (lispiec-service-type
            lispiec-configuration))


(define %lispiec-user
  (user-account
   (name "lispiec")
   (group "lispiec")
   (system? #t)
   (comment "lispiec user")
   (home-directory "/var/lib/lispiec")
   (shell (file-append shadow "/sbin/nologin"))))

(define %lispiec-group
  (user-group
   (name "lispiec")
   (system? #t)))

;; (define (serialize-string field-name value)
;;   #~(string-append #$(uglify-field-name field-name) " = " #$value "\n"))

;; (define (serialize-integer field-name value)
;;   #~(string-append #$(uglify-field-name field-name) " = " #$value "\n"))


(define-configuration/no-serialization lispiec-configuration
  (sbcl-lispiec
   (package sbcl-lispiec)
   "Package to use")
  (project-dir
   (string "/var/lib/lispiec/ieds/")
   "directory to scan for scd files")
  (default-port
    (integer 10000)
    "The start default port to run lispiec.")
  (scd-path
   (string "/var/lib/lispiec/scd.scd")
   "scd file to be used")
  (start-script
   (string "/var/lib/lispiec/start_rspace.lisp")
   "start lisp script")
  (use-goose
   (boolean #f)
   "Use Goose in container (must set spaciod as setuid)"))


(define (lispiec-shepherd-service config)
  (match-record config <lispiec-configuration> (sbcl-lispiec project-dir default-port start-script use-goose)
                (list (shepherd-service
                       (documentation "Run lispiec simulation.")
                       (requirement '(user-processes networking))
                       (provision '(sbcl-lispiec))
                       (start #~(make-forkexec-constructor
                                 (list
                                  ;; (if use-goose
                                  ;;     "/run/setuid-programs/spaciod")
                                  #$(file-append sbcl-lispiec "/bin/spaciod")
                                  "-d"
                                  #$project-dir
                                  (string-append "--additional-script="
                                                 #$start-script)
                                  )
                                 #:user "lispiec"
                                 #:group "lispiec"
                                 ;; #:log-file "/var/lib/lispiec/log.log"
                                 ))
                       (stop #~(make-kill-destructor))
                       ))))


(define (lispiec-activation config)
  (let ((sbcl-lispiec (lispiec-configuration-sbcl-lispiec config))
        (scd (lispiec-configuration-scd-path config))
        (project-dir (lispiec-configuration-project-dir config))
        (start-script (lispiec-configuration-start-script config)))
    #~(begin
        ;; If listen is a unix socket, create its parent directory.
        (unless (file-exists? "/var/lib/lispiec/ieds")
          (mkdir "/var/lib/lispiec/ieds"))
        (chown "/var/lib/lispiec/ieds" (passwd:uid (getpw "lispiec")) (passwd:gid (getpw "lispiec")))
        (when (file-exists? "/var/lib/lispiec/start_rspace.lisp")
          (delete-file "/var/lib/lispiec/start_rspace.lisp"))
        (when (file-exists? "/var/lib/lispiec/log.log")
          (delete-file "/var/lib/lispiec/log.log"))
        (if (file-exists? #$start-script)
          (copy-file #$start-script "/var/lib/lispiec/start_rspace.lisp")
          (when (file-exists? "/var/lib/lispiec/start_rspace.lisp")
            (delete-file "/var/lib/lispiec/start_rspace.lisp")))
        (unless (file-exists? "/var/lib/lispiec/start_rspace.lisp")
          (symlink #$(file-append sbcl-lispiec "/extra/start_rspace.lisp") "/var/lib/lispiec/start_rspace.lisp"))
        (when (file-exists? #$scd)
          (let* ((file1 #$scd)
                 (file2 "/var/lib/lispiec/ieds/scd.scd")
                 (b1 (file-exists? file1))
                 (b2 (file-exists? file2)))
            (if (and b1 b2)
                (let ((content1 (stat:size (stat file1 #f)))
                      (content2 (stat:size (stat file2 #f))))
                  (unless (equal? content1 content2)
                    (map delete-file (find-files #$project-dir "\\.scd$"))
                    (map delete-file (find-files #$project-dir "\\.SCD$"))
                    (map delete-file (find-files #$project-dir "\\.icd$"))
                    (map delete-file (find-files #$project-dir "\\.cfg$"))
                    (copy-file file1 file2)))
                (copy-file file1 file2)))))))

(define (lispiec-account config)
  (list %lispiec-group %lispiec-user))

(define (lispiec-profile config)
  (let ((sbcl-lispiec (lispiec-configuration-sbcl-lispiec config)))
    (list rlwrap bash coreutils sbcl sbcl-lispiec emacs emacs-sly)))

(define (lispiec-setuid config)
  (let ((goose (lispiec-configuration-use-goose config))
        (sbcl-lispiec (lispiec-configuration-sbcl-lispiec config)))
    (when goose (list (setuid-program
                       (program (file-append sbcl-lispiec "/bin/spaciod")))))))

(define lispiec-service-type
  (service-type
   (name 'lispiec)
   (extensions
    (list
     (service-extension shepherd-root-service-type lispiec-shepherd-service)
     (service-extension account-service-type lispiec-account)
     (service-extension profile-service-type lispiec-profile)
     (service-extension setuid-program-service-type lispiec-setuid)
     (service-extension activation-service-type lispiec-activation)))
   (description "Run lispiec.")
   (default-value (lispiec-configuration))))
