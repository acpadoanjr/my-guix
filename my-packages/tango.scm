(define-module (my-packages tango)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages avahi)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages check)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages base)
  #:use-module (gnu packages java)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages scanner)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages maven)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-build)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (gnu packages xml)
  #:use-module (guix download)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages image)
  #:use-module (guix git-download)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (guix build-system maven)
  #:use-module (guix build utils)
  #:use-module (guix build gnu-build-system)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system ant)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages curl)
  #:use-module (guix build-system texlive)
  #:use-module (guix svn-download)
  #:use-module (gnu packages python)
  #:use-module (guix build-system pyproject)
  #:use-module (gnu packages llvm)
  #:use-module (my-packages extra))


(define-public omniORB
  (let ((pp (package
              (name "omniORB")
              (version "4.2.3")
              (source (origin
                        (method url-fetch)
                        (file-name (string-append "omniORB-" version ".tar.bz2"))
                        (uri (string-append "https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-"
                                            version
                                            "/omniORB-"
                                            version
                                            ".tar.bz2/download"))
                        (sha256
                         (base32
                          "1jlb0wps6311dmhnphn64gv46z0bl8grch4fd9dcx5dlib02lh96"))))
              (build-system gnu-build-system)
              (arguments
               `(#:tests? #f
                 #:configure-flags
                 (list
                  "--disable-thread-tracing"
                  "--with-openssl"
                  "--disable-ipv6"
                  "--disable-static"
                  )
                 #:validate-runpath? #f
                 #:make-flags
                 (list
                  (string-append "LDFLAGS=-Wl,-rpath="
                                 (assoc-ref %outputs "out") "/lib")
                  )))
              (inputs `(("python" ,python)
                        ("pkg-config" ,pkg-config)
                        ("openssl" ,openssl)))
              (synopsis "Corba library")
              (description
               "omniORB is an Object Request Broker (ORB) which implements
specification 2.6 of the Common Object Request Broker Architecture
(CORBA).")
              (home-page "http://omniorb.sourceforge.net")
              (license gpl3+))))
    (let ((toolchain clang-toolchain-10))
      (package-with-c-toolchain pp `(("toolchain" ,toolchain))))))


(define-public tangoidl
  (let ((commit "ada81f822b9d24c0bb2d7d250896e9521368f681"))
    (package
      (name "tangoidl")
      (version "5.1.0")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/tango-idl.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "1k134irvrkrbj43rqlai8ryih3548fw4q7v9ppmh6bdkql1s0pxm"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         ;#:validate-runpath? #f
         ))
      (inputs
       `(
         ("omniorb" ,omniORB)
         ("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - idl")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))



(define-public cppTango
  (let ((commit "9.3.5"))
    (package
      (name "cppTango")
      (version "9.3.5")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/cppTango.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "11axhbqc8lmgndpakmc3yk4bjaxyh64l8jnxbyxzkkmcnf1qzz2a"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         #:configure-flags
         (list
          (string-append "-DIDL_BASE="
                         (assoc-ref %build-inputs "tangoidl")))
         ;; #:phases
         ;; (modifytphases %standardtphases
         ;;   (addtafter 'build 'pkg
         ;;     (lambda _
         ;;       (invoke (string-append "pkg-config --variable="
         ;;                              (assoc-ref %outputs "out")
         ;;                              " tango"))
         ;;       #true)))
         ;; #:test-target "run-tests"
         ))
      (inputs
       `(("python" ,python)
         ("python2" ,python-2.7)
         ("omniorb" ,omniORB)
         ("tangoidl" ,tangoidl)
         ("curl" ,curl)
         ("glibc" ,glibc)
         ("libcxx" ,libcxx)
         ("zeromq" ,zeromq)
         ("cppzmq" ,cppzmq)
         ("libjpeg-turbo" ,libjpeg-turbo)))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - shared library")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))

(define-public cppTango-log4tango
  (let ((commit "9.3.5"))
    (package
      (name "cppTango-log4tango")
      (version "9.3.5")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/cppTango.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "11axhbqc8lmgndpakmc3yk4bjaxyh64l8jnxbyxzkkmcnf1qzz2a"))
                ))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f
         ;; #:configure-flags
         ;; (list
         ;;  (string-append "-DIDL_BASE="
         ;;                 (assoc-ref %build-inputs "tangoidl")))

         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key source #:allow-other-keys)
               (begin
                 (mkdir "source")
                 (chdir "source")

                 ;; Preserve timestamps (set to the Epoch) on the copied tree so that
                 ;; things work deterministically.
                 (copy-recursively (string-append source "/log4tango") "."
                                   #:keep-mtime? #t)
                 ;; Make the source checkout files writable, for convenience.
                 (for-each (lambda (f)
                             (false-if-exception (make-file-writable f)))
                           (find-files ".")))
               ;; Extract the source tarball.
               ;(invoke "tar" "xvf" source)
               )))
         ;; (modifytphases %standardtphases
         ;;   (addtafter 'build 'pkg
         ;;     (lambda _
         ;;       (invoke (string-append "pkg-config --variable="
         ;;                              (assoc-ref %outputs "out")
         ;;                              " tango"))
         ;;       #true)))
         ;; #:test-target "run-tests"
         ))
      (inputs
       `(("python" ,python)
         ("python2" ,python-2.7)
         ("omniorb" ,omniORB)
         ("tangoidl" ,tangoidl)
         ("curl" ,curl)
         ("glibc" ,glibc)
         ("libcxx" ,libcxx)
         ("zeromq" ,zeromq)
         ("cppzmq" ,cppzmq)
         ("libjpeg-turbo" ,libjpeg-turbo)))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - shared library")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+))))


(define-public TangoDatabase
  (let ((commit "3482ed34f16465ac125809166b70dcd5ebd73cf6"))
    (package
      (name "TangoDatabase")
      (version "5.17")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/tango-controls/TangoDatabase.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "0ys05amrb5wgcbzipy32yjpxb7rhyzwnm5m00p5vh5f12lv2m6b3"))
                ))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f
         #:configure-flags
         (list
          (string-append "-DMYSQL_INCLUDE_DIR="
                         (assoc-ref %build-inputs "mariadbdev") "/include/mysql")
          "-DMYSQL=mariadb"
          "-DMYSQL_ADMIN=root"
          "-DTANGO_DB_NAME=tango"
          "-DMYSQL_HOST=localhost"
          (string-append "-DTANGO_PKG_INCLUDE_DIRS="
                         (assoc-ref %build-inputs "cppTango")
                         "/include/tango")
          (string-append "-DTANGO_PKG_LIBRARY_DIRS="
                         (assoc-ref %build-inputs "cppTango")
                         "/lib")
          (string-append "-DPKG_CONFIG_PATH="
                         (assoc-ref %build-inputs "cppTango")
                         "/lib/pkgconfig:"
                         (assoc-ref %build-inputs "mariadblib")
                         "/lib/pkgconfig")
          )
         )

       )
      (inputs
       `(("cppTango" ,cppTango)
         ("mariadbdev" ,mariadb "dev")
         ("mariadblib" ,mariadb "lib")
         ("mariadbout" ,mariadb "out")
         ("omniorb" ,omniORB)
         ("glibc" ,glibc)
         ("zeromq" ,zeromq)
         ("cppzmq" ,cppzmq)))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ))
      (synopsis "TANGO distributed control system - Tango Database")
      (description
       "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
      (home-page "http://www.tango-controls.org/")
      (license gpl3+)
      )))


(define-public tango-controls
  (let ((pp (package
             (name "tango-controls")
             (version "9.3.5")
             (source (origin
                      (method url-fetch)
                      (uri "https://gitlab.com/api/v4/projects/24125890/packages/generic/TangoSourceDistribution/9.3.5/tango-9.3.5.tar.gz")
                      (sha256
                       (base32
                        "1i59023gqm6sk000520y4kamfnfa8xqy9xwsnz5ch22nflgqn9px"))))
             (build-system gnu-build-system)
             (arguments
              `(#:tests? #f
                #:configure-flags
                (list
                 "--enable-mariadb"
                 "--with-mysql-ho=localhost"
                 "--disable-dbcreate"
                 (string-append "--with-mariadbclient-lib="
                                (assoc-ref %build-inputs "mariadblib")
                                "/lib")
                 (string-append "--with-mariadbclient-include="
                                (assoc-ref %build-inputs "mariadbdev")
                                "/include/mysql")
                 (string-append "--with-java="
                                (assoc-ref %build-inputs "icedtea")
                                "/bin/java")
                 (string-append "--with-omni="
                                (assoc-ref %build-inputs "omniorb"))
                 (string-append "--with-doxygen="
                                (assoc-ref %build-inputs "doxygen")
                                "/bin/doxygen")

                 )
                )
              )
             (inputs
              `(("python" ,python)
                ("python2" ,python-2.7)
                ("omniorb" ,omniORB)
                ("mariadbdev" ,mariadb "dev")
                ("mariadblib" ,mariadb "lib")
                ("mariadbout" ,mariadb "out")
                ("curl" ,curl)
                ("glibc" ,glibc)
                ("zlib" ,zlib "out")
                ("zeromq" ,zeromq)
                ("icedtea" ,icedtea-8 "jdk")
                ("doxygen" ,doxygen)
                ("cppzmq" ,cppzmq)
                ("pkg-config" ,pkg-config)))
             (native-search-paths
              (list (search-path-specification
                     (variable "CPLUS_INCLUDE_PATH")
                     (files (list "include/tango"
                                  "include/log4tango")))))
             (synopsis "TANGO distributed control system - shared libraries and java tools")
             (description
              "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
             (home-page "http://www.tango-controls.org/")
             (license gpl3+)
             )))
    (let ((toolchain clang-toolchain-10))
      (package-with-c-toolchain pp `(("toolchain" ,toolchain))))))


 (define-public yat
  (package
   (name "yat")
   (version "1.16.3")
   (source (origin
            (method git-fetch)
            (uri (git-reference  (url "https://gitlab.com/acpadoanjr/yat.git")
                                 (commit "master")))
            (sha256
             (base32
              "1ygrmm0fxim9ms0vfyd3h0dmzh2z7bvh25dxfrwkgswqc0xdv35r"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f
      )
    )
   ;; (inputs
   ;;  `(("autoconf" ,autoconf)
   ;;    ("automake" ,automake)
   ;;    ("autobuild" ,autobuild)
   ;;    ("libtool" ,libtool)))
   (synopsis "TANGO distributed control system - yat libraries")
   (description
    "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
   (home-page "http://www.tango-controls.org/")
   (license gpl3+)
   ))

(define-public yat4tango
  (package
   (name "yat4tango")
   (version "1.11.8")
   (source (origin
            (method git-fetch)
            (uri (git-reference  (url "https://gitlab.com/acpadoanjr/yat4tango.git")
                                 (commit "master")))
            (sha256
             (base32
              "1h6sh14003irkkagqrn4k2078dbgg2bsrwjxnqyyp38bq8p9a2bd"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f
      )
    )
   (inputs
    `(("cppTango" ,cppTango)
      ("yat" ,yat)
      ("pkg-config" ,pkg-config)
      ("zeromq" ,zeromq)
      ("cppzmq" ,cppzmq)
      ("omniorb" ,omniORB)))
   (synopsis "TANGO distributed control system - yat4tango libraries")
   (description
    "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
   (home-page "http://www.tango-controls.org/")
   (license gpl3+)
   ))

(define-public iec4tango
  (package
   (name "iec4tango")
   (version "0.2")
   (source (origin
            (method git-fetch)
            (uri (git-reference  (url "https://gitlab.com/acpadoanjr/iec4tango.git")
                                 (commit "master")))
            (sha256
             (base32
              "02m0vihmxabvcdpi68d5y52ncdjwjn1jn7s1651m6j7xh5lfpjyy"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags
      (list
       (string-append "-DTANGO_INCLUDE_PATH:PATH="
                      (assoc-ref %build-inputs "cppTango") "/include/tango")
       (string-append "-DLOG4TANGO_INCLUDE_PATH:PATH="
                      (assoc-ref %build-inputs "cppTango-log4tango") "/include/log4tango"))))
   (inputs
    `(("cppTango" ,cppTango)
      ("cppTango-log4tango" ,cppTango-log4tango)
      ;("tango-controls" ,tango-controls)
      ("yat" ,yat)
      ("yat4tango" ,yat4tango)
      ;("pkg-config" ,pkg-config)
      ("zeromq" ,zeromq)
      ("cppzmq" ,cppzmq)
      ("omniorb" ,omniORB)
      ("libiec61850" ,libiec61850)))
   (synopsis "TANGO distributed control system - iec4tango libraries")
   (description
    "TANGO is an object-oriented distributed control system. In TANGO all objects are representations of devices, which can be on the same computer or distributed over a network. Communication between devices can be synchronous, asynchronous or event driven.")
   (home-page "http://www.tango-controls.org/")
   (license gpl3+)
   ))

;; (define-public python-pint
;;   (package
;;     (name "python-pint")
;;     (version "0.20.1")
;;     (source (origin
;;               (method url-fetch)
;;               (uri (pypi-uri "Pint" version))
;;               (sha256
;;                (base32
;;                 "0rv0cbala7ibjbaf6kkcn0mdhqdbajnvlcw0f15gwzfwg10g0z1q"))))
;;     (build-system python-build-system)
;;     (native-inputs (list python-pytest python-pytest-cov python-pytest-mpl
;;                          python-pytest-subtests))
;;     (inputs (list python-setuptools-scm python-xarray python-dask python-distributed python-sparse))
;;     (home-page "https://github.com/hgrecco/pint")
;;     (synopsis "Physical quantities module")
;;     (description "Physical quantities module")
;;     (license license:bsd-3)))
;; (define-public python-taurus
;;   (package
;;     (name "python-taurus")
;;     (version "5.1.4")
;;     (source (origin
;;               (method url-fetch)
;;               (uri (pypi-uri "taurus" version))
;;               (sha256
;;                (base32
;;                 "1wvi4dfj9lf3ziyb5m3cy7lj2yjn3hr04bdb9imqlq8p0qmmbvid"))))
;;     (build-system python-build-system)

;;     (propagated-inputs (list python-click python-numpy python-packaging
;;                              python-pint python-pytango python-pyqt python-ply
;;                              python-lxml
;;                              ;python-guiqwt
;;                              python-pyqtwebengine qttools-5))
;;     (home-page "http://www.taurus-scada.org")
;;     (synopsis "A framework for scientific/industrial CLIs and GUIs")
;;     (description
;;      "This package provides a framework for scientific/industrial CLIs and GUIs")
;;     (license #f)))
(define-public python-taurus
  (package
   (name "python-taurus")
   (version "5.1.5")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "taurus" version))
            (sha256
             (base32
              "03n4sh3140af1d9mb1k90bgbz9l6axr7mq3irl2p8p0ppsnh43x6"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-click python-numpy python-packaging
                            python-pint))
   (home-page "http://www.taurus-scada.org")
   (synopsis "A framework for scientific/industrial CLIs and GUIs")
   (description
    "This package provides a framework for scientific/industrial CLIs and GUIs")
   (license #f)))

(define-public python-pytango
  (package
   (name "python-pytango")
   (version "9.3.5")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "pytango" version))
            (sha256
             (base32
              "00fijwwp8lvixm7piag66dmmx4qf4a29dx7swjr00mi0m9az79j1"))))
   (build-system python-build-system)
   (propagated-inputs (list python-six))
   (inputs `(("cppTango" ,cppTango)
             ("numpy" ,python-numpy)
             ("pkg" ,pkg-config)
             ("tdb" ,TangoDatabase)
             ("boost" ,boost)
             ("orb" ,omniORB)
             ("zeromq" ,zeromq)
             ("cppzmq" ,cppzmq)
             ("pytest-runner" ,python-pytest-runner)
             ("pytest-xdist" ,python-pytest-xdist)
             ("psutil" ,python-psutil)
             ("gevent" ,python-gevent)))
   (arguments
    `(#:tests? #f))
   (home-page "http://gitlab.com/tango-controls/pytango")
   (synopsis "A python binding for the Tango control system")
   (description
    "This package provides a python binding for the Tango control system")
   (license #f)))

(define-public python-pythonqwt
  (package
    (name "python-pythonqwt")
    (version "0.10.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "PythonQwt" version))
              (sha256
               (base32
                "1w421l47rjy43f8wqar6w22k4srv16cnpkymdvaxs1lb3iw3x4d4"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-numpy python-qtpy))
    (home-page "https://github.com/PierreRaybaut/PythonQwt")
    (synopsis "Qt plotting widgets for Python")
    (description "Qt plotting widgets for Python")
    (license #f)))

(define-public python-guidata
  (package
    (name "python-guidata")
    (version "2.3.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "guidata" version))
              (sha256
               (base32
                "068qmkg7vdbcr823vyz3gia783m4qq66i3xbg7nqnv57dj1090cx"))))
    (build-system pyproject-build-system)
    (arguments
     `(#:tests? #f))
    (propagated-inputs (list python-qtpy))
    (home-page "https://github.com/PierreRaybaut/guidata")
    (synopsis
     "Automatic graphical user interfaces generation for easy dataset editing and display")
    (description
     "Automatic graphical user interfaces generation for easy dataset editing and
display")
    (license #f)))

(define-public python-guiqwt
  (package
    (name "python-guiqwt")
    (version "4.3.3")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "guiqwt" version))
              (sha256
               (base32
                "0jnw623c6ipfmgvk8kxwh9jz8krmaxx0dizn052nhfpm7skx5w98"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-guidata
                             python-numpy
                             python-pillow
                             python-pythonqwt
                             python-qtpy
                             python-scipy))
    (home-page "https://github.com/PierreRaybaut/guiqwt")
    (synopsis
     "guiqwt is a set of tools for curve and image plotting (extension to PythonQwt)")
    (description
     "guiqwt is a set of tools for curve and image plotting (extension to PythonQwt)")
    (license #f)))


;; (define-public python-pythonqwt
;;   (package
;;     (name "python-pythonqwt")
;;     (version "0.10.2")
;;     (source (origin
;;               (method url-fetch)
;;               (uri (pypi-uri "PythonQwt" version))
;;               (sha256
;;                (base32
;;                 "1w421l47rjy43f8wqar6w22k4srv16cnpkymdvaxs1lb3iw3x4d4"))))
;;     (build-system python-build-system)
;;     (arguments
;;      `(#:tests? #f
;;        #:phases
;;        (modify-phases %standard-phases
;;                       (delete 'sanity-check))))
;;     (propagated-inputs (list python-numpy python-qtpy python-pyqt))
;;     (home-page "https://github.com/PierreRaybaut/PythonQwt")
;;     (synopsis "Qt plotting widgets for Python")
;;     (description "Qt plotting widgets for Python")
;;     (license #f)))
;; (define-public python-guidata
;;   (package
;;     (name "python-guidata")
;;     (version "2.3.0")
;;     (source (origin
;;               (method url-fetch)
;;               (uri (pypi-uri "guidata" version))
;;               (sha256
;;                (base32
;;                 "0nmr5fm5n2zb1xaybqbqmg5k0jbj5gg1nm4pljhm0zbpm7w6y2bf"))))
;;     (build-system python-build-system)
;;     (arguments
;;      `(#:tests? #f))
;;     (propagated-inputs (list python-qtpy))
;;     (inputs (list pkg-config))
;;     (home-page "https://github.com/PierreRaybaut/guidata")
;;     (synopsis
;;      "Automatic graphical user interfaces generation for easy dataset editing and display")
;;     (description
;;      "Automatic graphical user interfaces generation for easy dataset editing and
;; display")
;;     (license #f)))
;; (define-public python-guiqwt
;;   (package
;;     (name "python-guiqwt")
;;     (version "4.3.1")
;;     (source (origin
;;               (method url-fetch)
;;               (uri (pypi-uri "guiqwt" version))
;;               (sha256
;;                (base32
;;                 "1yvvb58pmk41ih19h8dywkn2x23r3qhmf4m4w3masbkv3yg5315v"))))
;;     (build-system python-build-system)
;;     (arguments
;;      `(#:tests? #f))
;;     (propagated-inputs (list python-guidata
;;                              python-numpy
;;                              python-pillow
;;                              python-pythonqwt
;;                              python-qtpy
;;                              python-pyqt
;;                              python-scipy))
;;     (inputs (list python-cython))
;;     (home-page "https://github.com/PierreRaybaut/guiqwt")
;;     (synopsis
;;      "guiqwt is a set of tools for curve and image plotting (extension to PythonQwt)")
;;     (description
;;      "guiqwt is a set of tools for curve and image plotting (extension to PythonQwt)")
;;     (license #f)))
