(define-module (my-packages extra)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages avahi)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages check)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages java)
  #:use-module (gnu packages time)
  #:use-module (gnu packages wget)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages autotools)
  #:use-module ((gnu packages compression) #:prefix comp:)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages scanner)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages maven)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages ocr)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build lisp-utils)
  #:use-module (guix download)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages image)
  #:use-module (guix git-download)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (guix build-system maven)
  #:use-module (guix build utils)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (guix build-system ant)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages guile)
  #:use-module (guix build-system texlive)
  #:use-module (guix build-system go)
  #:use-module (guix build-system asdf)
  #:use-module (guix svn-download)
  #:use-module (guix build utils)
  #:use-module (guix modules)
  #:use-module (guix config)
  #:use-module (guix gexp)
  #:use-module (guix store))


(define gtklp-bad-tool
  (package
    (name "my-gtklp")
    (version "1.3.4")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://sourceforge.net/projects/gtklp/files/gtklp/"
                                  version
                                  "/gtklp-"
                                  version
                                  ".src.tar.gz"))
              (sha256 (base32 "1arvnnvar22ipgnzqqq8xh0kkwyf71q2sfsf0crajpsr8a8601xy"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags
       (list
        "--disable-nls")
       #:phases
       (modify-phases %standard-phases
                      (add-after 'patch-source-shebangs 'fix
                                 (lambda _
                                   (let ((file "po/Makefile.in.in")
                                         (sh (string-append (assoc-ref %build-inputs "bash")
                                                            "/bin/bash")))
                                     (substitute* file
                                                  (("SHELL = /bin/sh")
                                                   (string-append "SHELL = "
                                                                  sh "\n"))))
                                   #true)))))
    (inputs
     `(("glib" ,glib)
       ("cups" ,cups)
       ("gtk+-2" ,gtk+-2)
       ("automake" ,automake)
       ("autobuild" ,autobuild)
       ("autoconf" ,autoconf)
       ("bash" ,bash)
       ("m4" ,m4)
       ("pkg-config" ,pkg-config)
       ))
    (home-page "https://sourceforge.net/projects/gtklp/")
    (synopsis "Graphical Frontend to CUPS")
    (description "GtkLP is a graphical Frontend to CUPS. It is intended to be easy to use, small and it should compile on many UniX-Systems.")
    (license license:gpl3+)))


(define-public python-cl4py
  (package
    (name "python-cl4py")
    (version "1.8.0")
    (source
      (origin
        (method git-fetch)
        (uri  (git-reference
               (url "https://github.com/marcoheisig/cl4py.git")
               (commit "19c0c1007493382836f188f0912e3c995c728ab9")))
        (sha256
         (base32 "1y3ymv8yqfn705cz6krvmv2cam2jpq360pxzgrg3gqsfxvwlnspq"))))
    (build-system python-build-system)
    (propagated-inputs (list python-numpy sbcl))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
                      (add-after 'install 'fix-file
                                 (lambda* (#:key outputs inputs #:allow-other-keys)
                                   (let* ((out (assoc-ref outputs "out"))
                                          (file "cl4py/py.lisp")
                                          (python-lib-path (string-append
                                                            (assoc-ref outputs "out")
                                                            "/lib/python"
                                                            "3.9"
                                                            "/site-packages/cl4py/")))
                                     (install-file file python-lib-path))
                                   #true)))))
    (home-page "https://github.com/marcoheisig/cl4py")
    (synopsis "Common Lisp for Python")
    (description "Common Lisp for Python")
    (license license:expat)))


(define-public libiec61850
  (let* ((myversion "v1.6.0"))
    (package
     (name "libiec61850")
     (version myversion)
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mz-automation/libiec61850.git")
                    (commit myversion)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "0y8p8h2gf5garyvab44rlfbiqnq7403g10z76bm0mlxx6xw2lm98"))))
     (build-system cmake-build-system)
     (arguments
      `(#:phases
        (modify-phases
         %standard-phases
         (add-after 'install 'copy-extra
                    (lambda* (#:key outputs inputs #:allow-other-keys)
                      (let* ((outf (assoc-ref outputs "out"))
                             (file1 "../source/tools/model_generator/genconfig.jar")
                             (file2 "../source/config/stack_config.h")
                             (target-path1 (string-append
                                            outf
                                            "/java/"))
                             (target-path2 (string-append
                                            outf
                                            "/include/libiec61850/")))
                        (install-file file1 target-path1)
                        (install-file file2 target-path2)
                        (copy-recursively "../source/src/iec61850/inc_private/" target-path2)
                        (copy-recursively "../source/src/mms/inc_private" target-path2)
                        (copy-recursively "../source/src/common/inc/" target-path2)
                        (copy-recursively "../source/hal/inc/" target-path2)
;;                         (with-imported-modules `((guix gexp))
;;                           (install-file (plain-file "genmodel.sh"
;;                                                     (string-append "#!/bin/sh
;; " openjdk "/bin/java -jar " outf "java/genmodel.jar $@")) "/bin/"))
                        ))))))
     ;; (inputs
     ;;  `(("openjdk" ,openjdk)))
     (home-page "https://github.com/mz-automation/libiec61850.git")
     (synopsis "IEC-61850 C library with python bindings")
     (description
      "libiec61850 is an open-source (GPLv3) implementation of an IEC 61850 client and server library implementing the protocols MMS, GOOSE and SV.")
     (license license:gpl3+))))

(define-public sbcl-lispiec
  (let* ((myversion "1.6.1"))
    (package
      (name "sbcl-lispiec")
      (version myversion)
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/acpadoanjr/lispiec.git")
                      (commit "v1.6.1")))
                (sha256
                 (base32 "1psb4gncjvlcwq9xlqgwk1d48d3wmvinx39sx124clgj6zb2v0cx"))
                (file-name "lispiec")))
      (build-system asdf-build-system/sbcl)
      (arguments
       (list
        #:phases
        #~(modify-phases
           %standard-phases
           (add-after 'unpack 'fix-paths
                      (lambda* (#:key outputs inputs #:allow-other-keys)
                        (substitute* "src/lispiec-functions.lisp"
                                     (("\"libiec61850.so\"")
                                      (string-append
                                       "\""
                                       (assoc-ref inputs "libiec61850") "/lib/libiec61850.so\"")))
                        (substitute* "ieds/generate.sh"
                                     (("java -jar genconfig.jar")
                                      (string-append
                                       (assoc-ref inputs "openjdk") "/bin/java -jar "
                                       (assoc-ref inputs "libiec61850") "/java/genconfig.jar")))
                        (substitute* "ieds/spacio"
                                     (("spacio.lisp")
                                      (string-append
                                       (assoc-ref outputs "out")
                                       "/bin/spacio.lisp")))
                        ))
           (add-after 'strip 'install-sh
                      (lambda* (#:key outputs inputs #:allow-other-keys)
                        (install-file "ieds/generate.sh"
                                      (string-append (assoc-ref outputs "out") "/bin/"))
                        (install-file "ieds/spacio"
                                      (string-append (assoc-ref outputs "out") "/bin/"))
                        (install-file "ieds/spacio.lisp"
                                      (string-append (assoc-ref outputs "out") "/bin/"))
                        (install-file "start_rspace.lisp"
                                      (string-append (assoc-ref outputs "out") "/extra/"))
                        ))
           (add-after 'install-sh 'build-program
                      (lambda* (#:key inputs outputs #:allow-other-keys)
                        (build-program
                         (string-append (assoc-ref outputs "out") "/bin/spaciod")
                         outputs
                         #:entry-program '((lispiec-rspace:main))
                         #:dependencies '("lispiec" "clack" "clack-handler-hunchentoot")
                         )))
           )))
      (propagated-inputs
       `(("sbcl-slynk" ,sbcl-slynk)
         ("libiec61850" ,libiec61850)
         ("sbcl-bordeaux-threads" ,sbcl-bordeaux-threads)
         ("sbcl-alexandria" ,sbcl-alexandria)
         ("sbcl-cl-utilities" ,sbcl-cl-utilities)
         ("sbcl-cffi" ,sbcl-cffi)
         ("sbcl-cffi-c-ref" ,sbcl-cffi-c-ref)
         ("sbcl-cl-utilities" ,sbcl-cl-utilities)
         ("sbcl-iterate" ,sbcl-iterate)
         ("sbcl-cl-csv" ,sbcl-cl-csv)
         ("sbcl-cl-ppcre" ,sbcl-cl-ppcre)
         ("sbcl-deeds" ,sbcl-deeds)
         ("sbcl-py4cl" ,sbcl-py4cl)
         ("sbcl-clog" ,sbcl-clog)
         ("sbcl-clack" ,sbcl-clack)
         ("sbcl-hunchentoot" ,sbcl-hunchentoot)
         ("sbcl-parachute" ,sbcl-parachute)
         ("sbcl-cxml-stp" ,sbcl-cxml-stp)
         ("sbcl-unix-opts" ,sbcl-unix-opts)
         ("sbcl-pzmq" ,sbcl-pzmq)
         ("sbcl-mssql" ,sbcl-mssql)
         ("sbcl-cl-json" ,sbcl-cl-json)
         ("sbcl-local-time" ,sbcl-local-time)
         ("xmlstarlet" ,xmlstarlet)
         ("sed" ,sed)
         ("gawk" ,gawk)
         ("coreutils" ,coreutils)
         ("iptables" ,iptables)
         ("rlwrap" ,rlwrap)
         ("python" ,python)
         ("python-opcua",python-opcua)
         ;; ("python-guibot" ,python-guibot)
         ))
      (inputs
       `(("openjdk" ,openjdk)))
      (home-page "https://gitlab.com/acpadoanjr/lispiec.git")
      (synopsis "IEC-61850 common-lisp library to simulate IEDs.")
      (description "IEC-61850 common-lisp library for simulation based on cffi and libiec61850.")
      (license license:gpl3+))))

(define-public cl-lispiec
  (sbcl-package->cl-source-package sbcl-lispiec))

(define-public python-pysmi
  (package
   (name "python-pysmi")
   (version "0.3.4")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "pysmi" version))
            (sha256
             (base32
              "0z4g9sgdaba0pnah3vb4h65lm0ihcg169qjvmdn3gs5f418a25dx"))))
   (build-system python-build-system)
   (propagated-inputs (list python-ply))
   (home-page "https://github.com/etingof/pysmi")
   (synopsis "SNMP SMI/MIB Parser")
   (description "SNMP SMI/MIB Parser")
   (license license:bsd-3)))

(define-public python-pysnmp
  (package
   (name "python-pysnmp")
   (version "4.4.12")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "pysnmp" version))
            (sha256
             (base32
              "1acbfvpbr45i137s00mbhh21p71ywjfw3r8z0ybcmjjqz7rbwg8c"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs (list python-pyasn1 python-pycryptodomex python-pysmi))
   (home-page "https://github.com/etingof/pysnmp")
   (synopsis "SNMP library for Python")
   (description "SNMP library for Python")
   (license license:bsd-3)))

(define-public python-snmpsim
  (package
   (name "python-snmpsim")
   (version "0.4.7")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "snmpsim" version))
            (sha256
             (base32
              "054vhpczafcg82b2z113h0nr1shh2lxmv73z2dw54ywfdznar51c"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs (list python-pysnmp))
   (home-page "https://github.com/etingof/snmpsim")
   (synopsis "SNMP Agents simulator")
   (description "SNMP Agents simulator")
   (license license:bsd-3)))


(define-public sbcl-data-table
  (let* ((myversion "0.1"))
    (package
      (name "sbcl-data-table")
      (version myversion)
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/AccelerationNet/data-table.git")
                      (commit "master")))
                (sha256
                 (base32 "0pin7hjsniz1ls3mrsjz6jqvlbbws8s4g7a0ks00dnwdm8nca6l3"))))
      (build-system asdf-build-system/sbcl)
      (arguments
       `(#:tests? #f))
      (propagated-inputs
       `(("sbcl-iterate" ,sbcl-iterate)
         ("sbcl-symbol-munger" ,sbcl-symbol-munger)
         ("sbcl-alexandria" ,sbcl-alexandria)
         ("cl-interpol" ,sbcl-cl-interpol)
         ))
      (home-page "https://github.com/AccelerationNet/data-table.git")
      (synopsis "A Common Lisp library providing a data-table data-structure that has rows of data and column names and types")
      (description "IEC-61850 common-lisp library for simulation based on cffi and libiec61850license")
      (license license:gpl3+))))

(define-public sbcl-cxml-stp
  (package
   (name "sbcl-cxml-stp")
   (version "1.0.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/sharplispers/cxml-stp.git")
                  (commit "master")))
            (sha256
             (base32 "01yfxxvb144i2mlp06fxx410mf3phxz5qaqvk90pp4dzdl883knv"))))
   (build-system asdf-build-system/sbcl)
   (propagated-inputs
    `(("sbcl-cxml" ,sbcl-cxml)
      ("sbcl-alexandria" ,sbcl-alexandria)
      ("sbcl-xpath" ,sbcl-xpath)))
   (home-page "https://github.com/sharplispers/cxml-stp.git")
   (synopsis "Fork of David Lichteblau's cxml-stp library")
   (description "Fork of David Lichteblau's cxml-stp library")
   (license license:gpl3)))

(define-public sbcl-slynk-client
  (package
   (name "sbcl-slynk-client")
   (version "1.0.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/Shookakko/slynk-client.git")
                  (commit "f232d4d")))
            (sha256
             (base32 "1yil8l8xscb81kz0a0xaybncmmp3r97wl5hlpfanj572z35yyfl5"))))
   (build-system asdf-build-system/sbcl)
   (propagated-inputs
    `(("sbcl-bordeaux-threads" ,sbcl-bordeaux-threads)
      ("sbcl-alexandria" ,sbcl-alexandria)
      ("sbcl-slynk" ,sbcl-slynk)
      ("sbcl-usocket" ,sbcl-usocket)))
   (home-page "https://github.com/Shookakko/slynk-client.git")
   (synopsis "Client side of the Slynk protocol.")
   (description "An implementation of the client side of Sly's Slynk debugging protocol.")
   (license license:gpl2)))


(define-public open62541
  (package
   (name "open62541")
   (version "1.3.4")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/open62541/open62541.git")
                  (commit "v1.3.4")))
            (sha256
             (base32 "0kbxm5c720j3k63gvqyl78cl7jiaci2r19w3k0l17yb92qg3lwaz"))))
   (build-system cmake-build-system)
   (arguments
    `(#:tests? #f
      #:configure-flags
      (list
       "-DBUILD_SHARED_LIBS=ON")))
   (inputs
    `(("python" ,python)
      ("mbedtls" ,mbedtls-apache)
      ("sphinx" ,python-sphinx)
      ("git" ,git)))
   (home-page "https://github.com/open62541/open62541.git")
   (synopsis "Open source implementation of OPC UA (OPC Unified Architecture) aka IEC 62541")
   (description
    "Open source implementation of OPC UA (OPC Unified Architecture) aka IEC 62541 licensed under Mozilla Public License v2.0")
   (license license:mpl2.0)))

(define-public python-asyncua
  (package
   (name "python-asyncua")
   (version "1.0.1")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "asyncua" version))
            (sha256
             (base32
              "1bbwfnigkjbqqw329iv6yn17ak7z2mh55kvxb28w7f92013l4lsi"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs (list python-aiofiles
                            python-aiosqlite
                            python-cryptography
                            python-dateutil
                            python-importlib-metadata
                            python-pytz
                            python-sortedcontainers))
   (home-page "http://freeopcua.github.io/")
   (synopsis "Pure Python OPC-UA client and server library")
   (description "Pure Python OPC-UA client and server library")
   (license #f)))

(define-public python-opcua-widgets
  (package
   (name "python-opcua-widgets")
   (version "0.6.1")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "opcua-widgets" version))
            (sha256
             (base32
              "12hzd2izil48si7mjgkl02v0piqy5wzq3j1hf984f673sb4sp213"))))
   (build-system python-build-system)
   (propagated-inputs (list python-asyncua python-pyqt))
   (home-page "https://github.com/FreeOpcUa/opcua-widgets")
   (synopsis "OPC-UA Widgets")
   (description "OPC-UA Widgets")
   (license #f)))

(define-public python-opcua-client
  (package
   (name "python-opcua-client")
   (version "0.8.4")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "opcua-client" version))
            (sha256
             (base32
              "0pjq79l6i08i7vmys472zvldhs1ijdi2vr355s3siwy7q95yq7cz"))))
   (build-system python-build-system)
   (propagated-inputs (list python-asyncua python-opcua-widgets python-pyqt))
   (home-page "https://github.com/FreeOpcUa/opcua-client-gui")
   (synopsis "OPC-UA Client GUI")
   (description "OPC-UA Client GUI")
   (license #f)))

(define-public python-opcua
  (package
   (name "python-opcua")
   (version "0.98.13")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "opcua" version))
            (sha256
             (base32
              "1rmxxg1w0d7z957nli6xp7yflpxakzqaly17m13331pdbw5z6lik"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs (list python-dateutil python-lxml python-pytz))
   (home-page "http://freeopcua.github.io/")
   (synopsis "Pure Python OPC-UA client and server library")
   (description "Pure Python OPC-UA client and server library")
   (license #f)))

(define-public python-pyspacio
  (package
   (name "python-pyspacio")
   (version "1.26")
   (source
    (origin
     (method git-fetch)
     (uri  (git-reference
            (url "https://gitlab.com/acpadoanjr/pyspacio.git")
            (commit "v1.26")))
     (sha256
      (base32 "16jgib347bl0bhkbgc41kpvczxhdr2nr8ngq8byn1f4sfzi87by8"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f))
   ;; (inputs `(("sbcl-lispiec" ,sbcl-lispiec)))
   (propagated-inputs (list python-cl4py python-opcua python-pydispatcher sbcl-lispiec sbcl-mssql libiec61850 sbcl))
   (home-page "https://gitlab.com/acpadoanjr/pyspacio.git")
   (synopsis "Lispiec wrapper for python")
   (description "Listiec wrapper for python")
   (license #f)))

(define-public webhook
  (package
   (name "webhook")
   (version "2.8.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/adnanh/webhook.git")
           (commit "2.8.1")
           ))
     (sha256
      (base32 "13p1x1mkk3lh5c8gljk5y5vk3arky84mcddyjqabl4k7dyk5bsph"))))
   (build-system go-build-system)
   (arguments
    `(#:import-path "github.com/adnanh/webhook"
      #:tests? #f))
   (home-page "/github.com/adnanh/webhook")
   (synopsis "webhook is a lightweight incoming webhook server to run shell commands")
   (description "webhook is a lightweight configurable tool written in Go, that allows you to easily create HTTP endpoints (hooks) on your server, which you can use to execute configured commands. You can also pass data from the HTTP request (such as headers, payload or query variables) to your commands. webhook also allows you to specify rules which have to be satisfied in order for the hook to be triggered.")
   (license #f)))

(define-public python-vncdotool
  (package
    (name "python-vncdotool")
    (version "1.2.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "vncdotool" version))
              (sha256
               (base32
                "03gacv8hd0qryq4vxaf17qar8rfa06dii27w4p2j37vzr8c8sh2k"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-pillow python-pycryptodomex python-twisted python-service-identity))
    (inputs (list python-pexpect))
    (home-page "http://github.com/sibson/vncdotool")
    (synopsis "Command line VNC client")
    (description "Command line VNC client")
    (license license:expat)))


(define-public python-guibot
  (package
    (name "python-guibot")
    (version "0.50.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/intra2net/guibot.git")
                    (commit "v0.50")))
              (sha256
               (base32
                "0j33sl3wy8glbi46bp53jspbgw4cgfqlnxh6241y22f7x75c8dq8"))))
    ;; (source (origin
    ;;           (method url-fetch)
    ;;           (uri (pypi-uri "guibot" version))
    ;;           (sha256
    ;;            (base32
    ;;             "08x77chsk4ap1nwgwcjjs7x8xq8y5hdbph7lnlpgcmvs6764lia3"))))
    (build-system python-build-system)
    (arguments (list
                #:tests? #f                     ;needs network
                #:phases #~(modify-phases %standard-phases
                             (replace 'build
                               (lambda _
                                 (with-directory-excursion "packaging"
                                   (invoke
                                    "python" "setup.py" "build"
                                    ))))
                             (replace 'install
                               (lambda*  (#:key outputs inputs #:allow-other-keys)
                                 (with-directory-excursion "packaging"
                                   (invoke
                                    "python" "setup.py" "install"
                                    "--single-version-externally-managed" "--root=/"
                                    (string-append "--prefix=" (assoc-ref outputs "out"))
                                    )))))))
    (propagated-inputs (list python-pillow python-vncdotool python-pytesseract xdotool opencv tesseract-ocr xwd imagemagick))
    (home-page "http://guibot.org")
    (synopsis "GUI automation tool")
    (description "GUI automation tool")
    (license #f)))

(define-public python-pytesseract
  (package
   (name "python-pytesseract")
   (version "0.3.10")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "pytesseract" version))
            (sha256
             (base32
              "03qjshmsdivdsjb6fyjpsp0yxrjv66wgalflhl81ml3zy2qaihzi"))))
   (build-system pyproject-build-system)
   (propagated-inputs (list python-packaging python-pillow tesseract-ocr))
   (home-page "https://github.com/madmaze/pytesseract")
   (synopsis "Python-tesseract is a python wrapper for Google's Tesseract-OCR")
   (description
    "Python-tesseract is a python wrapper for Google's Tesseract-OCR")
   (license #f)))


(define-public my-gtklp
  (let ((toolchain clang-toolchain-10 ;(specification->package "clang-toolchain@10")
                   ))
    (package-with-c-toolchain gtklp-bad-tool `(("toolchain" ,toolchain)))))

(define-public python-atlassian-python-api
  (package
   (name "python-atlassian-python-api")
   (version "3.41.4")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "atlassian-python-api" version))
            (sha256
             (base32
              "1yi7yf5nbpn2rnwxhkzx26kmhrrn01357c9ldqyl44rdpp11zr8h"))))
   (build-system pyproject-build-system)
   (arguments
    `(#:tests? #f))
   (propagated-inputs (list python-deprecated python-oauthlib python-requests
                            python-requests-oauthlib python-six))
   (home-page "https://github.com/atlassian-api/atlassian-python-api")
   (synopsis "Python Atlassian REST API Wrapper")
   (description "Python Atlassian REST API Wrapper")
   (license #f)))


(define-public python-pymssql
  (package
   (name "python-pymssql")
   (version "2.2.11")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pymssql" version))
     (sha256
      (base32 "06xcs2w6caq7ibwmpzqrw6684kiww8ipympgqig4gnwyzzqmp08m"))))
   (build-system pyproject-build-system)
   (home-page "")
   (inputs (list freetds))
   (propagated-inputs (list python-cython))
   (synopsis
    "DB-API interface to Microsoft SQL Server for Python. (new Cython-based version)")
   (description
    "DB-API interface to Microsoft SQL Server for Python. (new Cython-based version)")
   (license #f)))

(define-public sbcl-my-cl-autowrap
  (let ((revision "2")
        (commit "a5d71ebd7c21b87f449db1e16ab815750d7c0ea4"))
    ;; no taged branches
    (package
      (name "sbcl-my-cl-autowrap")
      (version (git-version "1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/rpav/cl-autowrap")
               (commit commit)))
         (file-name (git-file-name "cl-autowrap" version))
         (sha256
          (base32 "0795c817m1c41cz3ywzzg83z4pgkxdg6si553pay9mdgjvmrwmaw"))))
      (build-system asdf-build-system/sbcl)
      (arguments
       `(#:asd-systems '("cl-plus-c" "cl-autowrap" "cl-autowrap/libffi")
         #:phases
         (modify-phases
             %standard-phases
           (add-after 'unpack 'fix-paths
             (lambda* (#:key outputs inputs #:allow-other-keys)
               (substitute* "autowrap-libffi/library.lisp"
                 (("\"libffi.so\"")
                  (string-append
                   "\""
                   (assoc-ref inputs "libffi") "/lib/libffi.so\"")))
               ))
           )))
      (inputs
       `(("alexandria" ,sbcl-alexandria)
         ("cffi" ,sbcl-cffi)
         ("cl-json" ,sbcl-cl-json)
         ("cl-ppcre" ,sbcl-cl-ppcre)
         ("defpackage-plus" ,sbcl-defpackage-plus)
         ("trivial-features" ,sbcl-trivial-features)
         ("libffi", libffi)))
      (propagated-inputs (list c2ffi))
      (home-page "https://github.com/rpav/cl-autowrap")
      (synopsis "FFI wrapper generator for Common Lisp")
      (description "This is a c2ffi-based wrapper generator for Common Lisp.")
      (license license:bsd-2))))



(use-modules (guix packages)
             (guix download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages python)
             (gnu packages tls))

(define-public python-3.13
  (package
   (inherit python)
   (name "python-3.13")
   (version "3.13.0")
   (source (origin
            (method url-fetch)
            (uri (string-append "https://www.python.org/ftp/python/"
                                version "/Python-" version ".tar.xz"))
            (sha256
             (base32 "0zdhxxchfskzhbgrrnj3rn702j1f5r95g154vka11crw5s4fav88"))))))

(define-public python-backports-asyncio-runner
  (package
   (name "python-backports-asyncio-runner")
   (version "1.1.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "backports.asyncio.runner" version))
     (sha256
      (base32 "1dbywca3nkhfks6jic15m62n10hqid7mxasn7ixk2x32v77gin9y"))))
   (build-system pyproject-build-system)
   (home-page "")
   (synopsis
    "Backport of asyncio.Runner, a context manager that controls event loop life cycle.")
   (description
    "Backport of asyncio.Runner, a context manager that controls event loop life
cycle.")
   (license #f)))

(define-public sbcl-homem
  (let* ((myversion "1.0.0"))
    (package
      (name "sbcl-homem")
      (version myversion)
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/acpadoanjr/ohomemeterno2.git")
                      (commit "master")))
                (sha256
                 (base32 "0sbiswwy43rml0lskznml82bmq9c5596fismxlrxz54rvfmhxvy8"))
                ))
      (build-system asdf-build-system/sbcl)
      (arguments
       (list
        #:phases
        #~(modify-phases
           %standard-phases
           (add-after 'unpack 'fix-paths
                      (lambda* (#:key outputs inputs #:allow-other-keys)
                        (substitute* "ohomeneterno.lisp"
                                     ((":root \"static/\"")
                                      (string-append
                                       ":root \""
                                       (assoc-ref outputs "out") "/share/common-lisp/sbcl/homem/static/\"")))
                        (substitute* "ohomeneterno.lisp"
                                     (("\\(sb-posix:getcwd\\)")
                                      (string-append
                                       "\""
                                       (assoc-ref outputs "out") "/share/common-lisp/sbcl/homem/\"")))
                        ))
           (add-after 'strip 'build-program
                      (lambda* (#:key inputs outputs #:allow-other-keys)
                        (build-program
                         (string-append (assoc-ref outputs "out") "/bin/homemd")
                         outputs
                         #:entry-program '((homem:main))
                         #:dependencies '("homem" "clack" "lack" "clack-handler-hunchentoot")
                         ))))))
      (propagated-inputs
       `(
         ("sbcl-clack" ,sbcl-clack)
         ("sbcl-lack" ,sbcl-clack)
         ("sbcl-hunchentoot" ,sbcl-hunchentoot)
         ("sbcl-spinneret" ,sbcl-spinneret)
         ("sbcl-ningle" ,sbcl-ningle)
         ))
      (home-page "https://gitlab.com/acpadoanjr/ohomemeterno2.git")
      (synopsis "Blog perso.")
      (description "Blog perso")
      (license license:gpl3+))))
